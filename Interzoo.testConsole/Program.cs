﻿using Interzoo.DAL;
using Interzoo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interzoo.testConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            //AccessPersonne AP = new AccessPersonne();
            //AP.getOne(1);

            Personne P = new Personne();
            ////AP.Insert(P);
            P.Nom = "Jones";
            //AP.Update(P);
            //AP.Delete(P);

            Dictionary<string, object> MaPersonne = new Dictionary<string, object>();
            MaPersonne["Nom"] = P.Nom;

            DBConnect cb = new DBConnect();
            string requete = @"INSERT INTO [dbo].[Personne]
                               ([Nom]
                               ,[Prenom]
                               ,[DateNaiss]
                               ,[CodePostal]
                               ,[Localite])
                         VALUES
                               ('Jones'
                               ,'Hank'
                               ,null
                               ,1200
                               ,'Bruxelles')";


            cb.Insert(MaPersonne,requete);

            AccessVisiteur AV = new AccessVisiteur();
            AV.getAll();
            //AV.Insert(P);

            //AV.Delete(2);
            //AV.Update(2);

            Console.ReadLine();
        }
    }
}
