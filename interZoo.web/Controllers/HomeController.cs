﻿using interZoo.web.Models;
using Interzoo.Models.ModelBilleterie;
using Interzoo.Models.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace interZoo.web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {

            List<Categorie_ticket> lg = Categorie_ticket_repo.Get() ;

            return View(lg);
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}