﻿using interZoo.web.Models;
using interZoo.web.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace interZoo.web.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Authenticate()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Authenticate(Models.Login monLogin)  // le systeme crée automatiquement un new object Login et rempli ses propriétés avec les valeurs des inputs si le nom des inputs correspondent aux noms des propriétés
        {
            //Models.Login l = new Models.Login();  // on doit préciser nom du dom car login à le même nom que la variable => il doit pouvoir s'y retrouver
            if (monLogin.check())
            {
                //ok
                //Session["Login"] = Login;
                SessionUtils.ConnectedUser = monLogin;
                return RedirectToAction("Index", "Home");  //demande d'exécuter la fonction Index qui se trouve dans le Home Controller
            }
            else
            {
                //ko
                ViewBag.Message = "Erreur Login/Mot de passe";
            }

            return View();
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(Models.NewPersonne mapersonne)
        {
            
            if (mapersonne.insert()>0)
            {
                Login monlogin = new Login();
                monlogin.Pseudo = mapersonne.Login;
                //monlogin.Nom = mapersonne.Nom;
                //monlogin.Prenom = mapersonne.Prenom;

                SessionUtils.ConnectedUser=monlogin ;
                return RedirectToAction("MyAccount", "Account");  
            }
            else
            {
                //ko
                //ViewBag.Message = "Erreur vous avez mal complété le formulaire";
                return RedirectToAction("Register", "Account");
            }

            return View();
        }


        public ActionResult MyAccount()
        {
            NewPersonne rm = new NewPersonne();
            rm.Nom = "Bebert";
            rm.Prenom = "LAFrite";


            return View(rm);
        }

        [HttpPost]
        public ActionResult MyAccount(NewPersonne rm) //par le post on reçoit un register model
        {
            //Enregistrer en db

            return View(rm);
        }

        public ActionResult Logout()
        {
            Session.Abandon();
            return RedirectToAction("Index", "Home");
        }
    }
}