﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace interZoo.web.Areas.UserLog.Controllers
{
    public class HomeController : Controller
    {
        // GET: UserLog/Home
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Retour()
        {
            //pour sortir et retourner vers le site principal
            return RedirectToAction("Index", new { controller = "Home", area = "" });
        }
        public ActionResult Achats()
        {
            return View();
        }
        public ActionResult MonCompte()
        {

            return View();
        }
    }
}