﻿using interZoo.web.Areas.UserLog.Models;
using interZoo.web.Areas.UserLog.Models.UserRepository;
using interZoo.web.Tools;
using Interzoo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace interZoo.web.Areas.UserLog.Controllers
{
    public class AchatTicketController : Controller
    {
        // GET: UserLog/AchatTicket
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Step2Get()
        {
            return View("Step2",SessionUtils.InfoReservation); //si on veut renvoyer une vue d'un autre nom que celui de la méthode 
                                                        //on doit le mettre en paramètre de View() après la virgule c'est ce qu'on passe à la vue
        }


        [HttpPost]
        public ActionResult Step2()
        {
            int nbr;
            int.TryParse(Request.Form.Get("qutte"),out nbr); //ce qu'on récupère du formulaire de la page précédente en Get
                                                            //"qutte" est le nom de l'input ciblé
            FormModel fm = new FormModel();

            if (nbr >= 0 && nbr<9)
            {
                fm.Quantite = nbr;
                SessionUtils.InfoReservation = fm;
                return View(fm);

            }

            else
            {
                return View();
            }
            
        }

        [HttpPost]
        public ActionResult Step3()
        {
            //Request.Form.Get("qutte")
            List<CreateTicket> listTicket = new List<CreateTicket>();
            
            for(int i=0;i<SessionUtils.InfoReservation.Quantite;i++)
            {
                CreateTicket ct = new CreateTicket();
                ct.Nom = Request.Form.Get("nom_" + i);
                ct.Prenom = Request.Form.Get("prenom_" + i);
                double prixbis;
                double.TryParse(Request.Form.Get("inputcach_" + i),out prixbis);
                ct.Prix = prixbis;
                int monid;
                int.TryParse(Request.Form.Get("categorie_" + i), out monid);
                ct.Id_cat=monid;

                listTicket.Add(ct);
            }
            SessionUtils.ListeDesTickets = listTicket;

            return View();
        }

        [HttpPost]
        public ActionResult Step4()
        {
            CreateReservation mareservation = new CreateReservation();
            
            DateTime madate;
            DateTime.TryParse(Request.Form.Get("Date_reservation"), out madate);
            mareservation.Date_reservation = madate;
            mareservation.IdPersonne = SessionUtils.ConnectedUser.IdPersonne;
            mareservation.Lt = SessionUtils.ListeDesTickets;

            SessionUtils.UneReservation = mareservation;

            return View();
        }

        public ActionResult Paiement()
        {
            //SessionUtils.UneReservation.IdPersonne = SessionUtils.ConnectedUser.IdPersonne;
            int idreservation=CreateReservation_repo.Insert(SessionUtils.UneReservation); 
            foreach(CreateTicket tick in SessionUtils.UneReservation.Lt)
            {
                CreateTicket_repo.Insert(tick,idreservation);
            }
            
            TempData["messageConfirmation"]="Votre commande a été enregistrée!";


            return RedirectToAction("Index", "Home");
        }


    }
}