﻿using Interzoo.Models.ModelBilleterie;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace interZoo.web.Areas.UserLog.Models.UserMapper
{
    public static class TicketMapper
    {
        public static Ticket ToTicket(CreateTicket ct)
        {
            Ticket t = new Ticket();

            t.Nom = ct.Nom;
            t.Prenom = ct.Prenom;
            t.Prix = ct.Prix;
            t.Id_cat = ct.Id_cat;
            

            return t;     
        }

        public static Reservation ToReservation(CreateReservation cr)
        {
            Reservation r = new Reservation();

            r.IdPersonne = cr.IdPersonne;
            r.Date_reservation = cr.Date_reservation;
            r.Nbr_ticket = cr.Lt.Count;

            return r;    

        }

        //public static CreateReservation ToCreateReservation (Reservation r)
        //{
        //    CreateReservation cr = new CreateReservation();
            


        //}


    }
}