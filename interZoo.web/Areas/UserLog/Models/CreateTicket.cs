﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace interZoo.web.Areas.UserLog.Models
{
    public class CreateTicket
    {
        private string nom;
        private string prenom;
        private double prix;
        private int id_cat;



        public string Nom
        {
            get
            {
                return nom;
            }

            set
            {
                nom = value;
            }
        }

        public string Prenom
        {
            get
            {
                return prenom;
            }

            set
            {
                prenom = value;
            }
        }

        public double Prix
        {
            get
            {
                return prix;
            }

            set
            {
                prix = value;
            }
        }

        public int Id_cat
        {
            get
            {
                return id_cat;
            }

            set
            {
                id_cat = value;
            }
        }
    }
}