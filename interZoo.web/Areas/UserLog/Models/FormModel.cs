﻿using interZoo.web.Models;
using interZoo.web.Models.Web_repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace interZoo.web.Areas.UserLog.Models
{
    public class FormModel
    {
        private int quantite;
        public List<Cat_TicketDisplay> liste_ticket_display;

        public int Quantite
        {
            get
            {
                return quantite;
            }

            set
            {
                quantite = value;
            }
        }

        public FormModel()
        {
            liste_ticket_display = new List<Cat_TicketDisplay>();
            liste_ticket_display.AddRange(Cat_ticketDisplay_repo.Get_ticket_display());
        }

    }
}