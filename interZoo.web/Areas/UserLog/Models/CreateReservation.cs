﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace interZoo.web.Areas.UserLog.Models
{
    public class CreateReservation
    {
        private int idPersonne;
        private DateTime date_reservation;
        private List<CreateTicket> lt;

        public int IdPersonne
        {
            get
            {
                return idPersonne;
            }

            set
            {
                idPersonne = value;
            }
        }


        public DateTime Date_reservation
        {
            get
            {
                return date_reservation;
            }

            set
            {
                date_reservation = value;
            }
        }

        public List<CreateTicket> Lt
        {
            get
            {
                return lt;
            }

            set
            {
                lt = value;
            }
        }
    }
}