﻿using interZoo.web.Areas.UserLog.Models.UserMapper;
using Interzoo.Models.ModelBilleterie;
using Interzoo.Models.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace interZoo.web.Areas.UserLog.Models.UserRepository
{
    public static class CreateTicket_repo
    {
        public static int Insert(CreateTicket ct, int id_reservation)
        {
            Ticket t = new Ticket();

            t = TicketMapper.ToTicket(ct);

            t.Id_reservation = id_reservation;

            return Ticket_repo.Insert_ticket(t);
        }
    }
}