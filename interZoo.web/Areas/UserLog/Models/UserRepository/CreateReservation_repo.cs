﻿using interZoo.web.Areas.UserLog.Models.UserMapper;
using Interzoo.Models.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace interZoo.web.Areas.UserLog.Models.UserRepository
{
    public static class CreateReservation_repo
    {
        public static int Insert(CreateReservation cr)
        {
            return Reservation_repo.Insert_reservation(TicketMapper.ToReservation(cr));
            
        }

    }
}