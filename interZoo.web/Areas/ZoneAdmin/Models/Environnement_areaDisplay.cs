﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace interZoo.web.Areas.ZoneAdmin.Models
{
    public class Environnement_areaDisplay
    {
        private int _id_envi;
        private string _type_envi;


        public Environnement_areaDisplay()
        { }

        public Environnement_areaDisplay(int id_envi, string type_envi)
        {
            this._id_envi = id_envi;
            this._type_envi = type_envi;
        }

        public int Id_envi
        {
            get
            {
                return _id_envi;
            }

            set
            {
                _id_envi = value;
            }
        }

        public string Type_envi
        {
            get
            {
                return _type_envi;
            }

            set
            {
                _type_envi = value;
            }
        }
    }
}