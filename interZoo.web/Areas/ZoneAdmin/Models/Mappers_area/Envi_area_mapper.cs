﻿using Interzoo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace interZoo.web.Areas.ZoneAdmin.Models.Mappers_area
{
    public static class Envi_area_mapper
    {
        public static Environnement_areaDisplay MapEnvi_to_EnviAreaDisplay(Environnement e)
        {
            return new Environnement_areaDisplay
            {
                Id_envi = e.Id_envi,
                Type_envi = e.Type_envi
            };
        }
}
}