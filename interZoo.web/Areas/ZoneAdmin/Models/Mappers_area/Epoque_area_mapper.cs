﻿using Interzoo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace interZoo.web.Areas.ZoneAdmin.Models.Mappers_area
{
    public static class Epoque_area_mapper
    {
        public static Epoque_areaDisplay MapEpoque_to_EpoqueAreaDisplay(Epoque e)
        {
            return new Epoque_areaDisplay
            {
                Id_epoque = e.Id_epoque,
                Nom_epoque = e.Nom_epoque
            };
        }
    }
}