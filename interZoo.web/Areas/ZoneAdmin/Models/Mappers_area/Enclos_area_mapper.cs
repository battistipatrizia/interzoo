﻿using interZoo.web.Areas.ZoneAdmin.Models;
using Interzoo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace interZoo.web.Areas.ZoneAdmin.Models.Mappers_area
{
    public static class Enclos_area_mapper
    {
        public static Enclos MapEnclosInsert_to_Enclos(Enclos_insert ei)
        {
            return new Enclos
            { 
                Id_enclos = ei.Id_enclos,
                Taille = ei.Taille,
                Capacite_accueil = ei.Capacite_accueil,
                Id_envi = ei.Id_envi,
                Id_epoque = ei.Id_epoque,
                Details_enclos = ei.Details_enclos
            };
        }

        public static Enclos_insert MapEnclos_to_EnclosInsert (Enclos e)
        {
            return new Enclos_insert
            {
                Id_enclos = e.Id_enclos,
                Taille = e.Taille,
                Capacite_accueil = e.Capacite_accueil,
                Id_envi = e.Id_envi,
                Id_epoque = e.Id_epoque,
                Details_enclos = e.Details_enclos
            };
        }

        public static Enclos_areaDisplay Enclos_to_EnclosAreaDisplay(Enclos e)
        {
            return new Enclos_areaDisplay
            {
                Id_enclos = e.Id_enclos,
                Taille = e.Taille,
                Capacite_accueil = e.Capacite_accueil,
                Id_envi = e.Id_envi,
                Id_epoque = e.Id_epoque,
                Details_enclos = e.Details_enclos
            };
        }

        public static Enclos Enclos_to_EnclosAreaDisplay(Enclos_areaDisplay ea)
        {
            return new Enclos
            {
                Id_enclos = ea.Id_enclos,
                Taille = ea.Taille,
                Capacite_accueil = ea.Capacite_accueil,
                Id_envi = ea.Id_envi,
                Id_epoque = ea.Id_epoque,
                Details_enclos = ea.Details_enclos
            };
        }
    }
}