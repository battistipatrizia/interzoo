﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace interZoo.web.Areas.ZoneAdmin.Models
{
    public class Update_e_EnclosDisplay_general
    {
        private Enclos_areaDisplay _current_enclos;
        private Recherche_e_display _red;

        public Update_e_EnclosDisplay_general(Enclos_areaDisplay current_enclos)
        {
            this.Current_enclos = current_enclos;
        }

        public Enclos_areaDisplay Current_enclos
        {
            get
            {
                return _current_enclos;
            }

            set
            {
                _current_enclos = value;
            }
        }

        public Recherche_e_display Red
        {
            get
            {
                Recherche_e_display red = new Recherche_e_display();
                return red;
            }

            //set
            //{
            //    _red = value;
            //}
        }

    }
}