﻿using interZoo.web.Areas.ZoneAdmin.Models.Admin_repository;
using interZoo.web.Models;
using interZoo.web.Models.Web_repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace interZoo.web.Areas.ZoneAdmin.Models
{
    public class Enclos_areaDisplay
    {
        private int _id_enclos;
        private double _taille;
        private double _capacite_accueil;
        private int _id_epoque;
        private string _nom_epoque;
        private int _id_envi;
        private string _type_envi;
        private string _details_enclos;
        private List<AnimalDisplay> _liste_animalsDisplay; //même si pas dans le cosntructeur, pas de Priopriétés si pas de variable décléré avant

        public Enclos_areaDisplay()
        { }

        public Enclos_areaDisplay(int id_enclos, double taille, double capacite_accueil, int id_epoque, int id_envi, string details_enclos)
        {
            this._id_enclos = id_enclos;
            this._taille = taille;
            this._capacite_accueil = capacite_accueil;
            this._id_epoque = id_epoque;
            this._id_envi = id_envi;
            this._details_enclos = details_enclos;
        }

        public int Id_enclos
        {
            get
            {
                return _id_enclos;
            }

            set
            {
                _id_enclos = value;
            }
        }

        public double Taille
        {
            get
            {
                return _taille;
            }

            set
            {
                _taille = value;
            }
        }

        public double Capacite_accueil
        {
            get
            {
                return _capacite_accueil;
            }

            set
            {
                _capacite_accueil = value;
            }
        }

        public int Id_epoque
        {
            get
            {
                return _id_epoque;
            }

            set
            {
                _id_epoque = value;
            }
        }


        public string Nom_epoque
        {
            get
            {
                return Epoque_areaDisplay_repo.GetOne_epoque_areaDisplay(Id_epoque).Nom_epoque;
            }

            //set
            //{
            //    _nom_epoque = value;
            //}
        }

        public int Id_envi
        {
            get
            {
                return _id_envi;
            }

            set
            {
                _id_envi = value;
            }
        }

        public string Type_envi
        {
            get
            {
                return Environnement_areaDisplay_repo.GetOne_envi_areaDisplay(Id_envi).Type_envi;
                ;
            }

            //set
            //{
            //    _type_envi = value;
            //}
        }

        public string Details_enclos
        {
            get
            {
                return _details_enclos;
            }

            set
            {
                _details_enclos = value;
            }
        }

        public List<AnimalDisplay> Liste_animalsDisplay
        {
            get
            {
                string query_where = "Id_enclos = @Id_enclos";
                Dictionary<string, Object> parametres_where = new Dictionary<string, object>();
                parametres_where.Add("@Id_enclos", Id_enclos);
                return AnimalDisplay_repo.GetFilter_animal_display(query_where, parametres_where);
            }

            //set
            //{
            //    _liste_animalsDisplay = value;
            //}
        }
    }
}