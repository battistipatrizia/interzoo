﻿using interZoo.web.Areas.ZoneAdmin.Models.Admin_repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace interZoo.web.Areas.ZoneAdmin.Models
{
    public class Recherche_e_display
    {
        private List<Environnement_areaDisplay> _liste_enviAreaDisplay;
        private List<Epoque_areaDisplay> _list_epoqueAreaDisplay;

        public List<Environnement_areaDisplay> Liste_enviAreaDisplay
        {
            get
            {
                return Environnement_areaDisplay_repo.Get_envi_areaDisplay();
            }

            //set
            //{
            //    _liste_enviAreaDisplay = value;
            //}
        }

        public List<Epoque_areaDisplay> List_epoqueAreaDisplay
        {
            get
            {
                return Epoque_areaDisplay_repo.Get_epoque_areaDisplay();
            }

            //set
            //{
            //    _list_epoqueAreaDisplay = value;
            //}
        }
    }
}