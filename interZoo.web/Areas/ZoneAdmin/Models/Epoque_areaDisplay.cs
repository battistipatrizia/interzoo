﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace interZoo.web.Areas.ZoneAdmin.Models
{
    public class Epoque_areaDisplay
    {

        private int _id_epoque;
        private string _nom_epoque;

        public Epoque_areaDisplay()
        { }


        //public Epoque_areaDisplay(int id_epoque, string nom_epoque)
        //{
        //    this.Id_epoque = id_epoque;
        //    this.Nom_epoque = nom_epoque;
        //}

        public int Id_epoque
        {
            get
            {
                return _id_epoque;
            }

            set
            {
                _id_epoque = value;
            }
        }

        public string Nom_epoque
        {
            get
            {
                return _nom_epoque;
            }

            set
            {
                _nom_epoque = value;
            }
        }

}
}