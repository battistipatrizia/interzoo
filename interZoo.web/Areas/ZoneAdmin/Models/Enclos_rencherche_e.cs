﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace interZoo.web.Areas.ZoneAdmin.Models
{
    public class Enclos_rencherche_e
    {
        private List<int> _liste_tailles;
        private List<int> _liste_capacites;
        private Recherche_e_display _listes_epoques_et_envi;

        public List<int> Liste_tailles
        {
            get
            {
                List<int> l = new List<int>();

                l.Add(250);
                l.Add(500);
                l.Add(750);
                l.Add(1000);
                l.Add(1250);
                l.Add(1500);
                return l;
            }

            //set
            //{
            //    _liste_tailles = value;
            //}
        }

        public List<int> Liste_capacites
        {
            get
            {

                List<int> l = new List<int>();

                l.Add(1);
                l.Add(6);
                l.Add(11);
                l.Add(16);
                l.Add(21);
                return l;
            }

            //set
            //{
            //    _liste_capacites = value;
            //}
        }

        public Recherche_e_display Listes_epoques_et_envi
        {
            get
            {
                return new Recherche_e_display();
            }

            //set
            //{
            //    _liste_envi = value;
            //}
        }


        //private List<Regimes>
    }
}