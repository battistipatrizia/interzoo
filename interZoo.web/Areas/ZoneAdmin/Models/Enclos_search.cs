﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace interZoo.web.Areas.ZoneAdmin.Models
{
    public class Enclos_search
    {
        private int _taille_supp;
        private int _capacite_supp;
        private int _id_envi;
        private int _id_epoque;
        private string _query_where;
        private Dictionary<string, object> _parametres_where;

        public int Taille_supp
        {
            get
            {
                return _taille_supp;
            }

            set
            {
                _taille_supp = value;
            }
        }

        public int Capacite_supp
        {
            get
            {
                return _capacite_supp;
            }

            set
            {
                _capacite_supp = value;
            }
        }

        public int Id_envi
        {
            get
            {
                return _id_envi;
            }

            set
            {
                _id_envi = value;
            }
        }

        public int Id_epoque
        {
            get
            {
                return _id_epoque;
            }

            set
            {
                _id_epoque = value;
            }
        }

        public string Query_where
        {
            get
            {
                _query_where = "";
                //partie string taille
                if (Taille_supp == 1500) 
                {
                    _query_where += "Taille > @Taille";

                    if (Capacite_supp != 0 || Id_envi != 0 || Id_epoque != 0)
                    {
                        _query_where += " AND ";
                    }
                }
                else if(Taille_supp != 0)
                {
                    _query_where += "Taille > @Taille_inf AND Taille <= @Taille";

                    if (Capacite_supp != 0 || Id_envi != 0 || Id_epoque != 0)
                    {
                        _query_where += " AND ";
                    }
                }

                //partie string capacite
                if (Capacite_supp == 21)
                {
                    _query_where += "Capacite_accueil > @Capacite_accueil";

                    if (Id_envi != 0 || Id_epoque != 0)
                    {
                        _query_where += " AND ";
                    }
                }
                else if (Capacite_supp != 0)
                {
                    _query_where += "Capacite_acceuil > @Capacite_accueil_inf AND Capacite_accueil <= @Capacite_accueil";

                    if (Id_envi != 0 || Id_epoque != 0)
                    {
                        _query_where += " AND ";
                    }
                }

                //partie string envi
                if (Id_envi != 0)
                {
                    _query_where += "Id_envi = @Id_envi";

                    if (Id_epoque != 0)
                    {
                        _query_where += " AND ";
                    }

                }

                //partie string epoque
                if (Id_epoque != 0)
                {
                    _query_where += "Id_epoque = @Id_epoque";

                }

                return _query_where;
            }

            //set
            //{
            //    _query_where = value;
            //}
        }

        public Dictionary<string, object> Parametres_where
        {
            get
            {
                _parametres_where = new Dictionary<string, object>();

                if (Taille_supp != 0)
                {
                    _parametres_where.Add("@Taille", Taille_supp);

                    if (Taille_supp != 1500)
                    {
                        _parametres_where.Add("@Taille_inf", (Taille_supp - 250));
                    }
                }

                if (Capacite_supp != 0)
                {
                    _parametres_where.Add("@Capacite_accueil", Capacite_supp);

                    if (Capacite_supp != 21)
                    {
                        _parametres_where.Add("@Capacite_accueil_inf", (Capacite_supp - 4));
                    }
                }

                if (Id_envi != 0)
                {
                    _parametres_where.Add("@Id_envi", Id_envi);
                }

                if (Id_epoque != 0)
                {
                    _parametres_where.Add("@Id_epoque", Id_epoque);
                }

                return _parametres_where;
            }

            //set
            //{
            //    parametres_where = value;
            //}
        }
    }
}