﻿using interZoo.web.Areas.ZoneAdmin.Models.Mappers_area;
using Interzoo.Models;
using Interzoo.Models.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace interZoo.web.Areas.ZoneAdmin.Models.Admin_repository
{
    public class Enclos_areaDisplay_repo
    {

        public static Enclos_areaDisplay GetOne_EnclosAreaDisplay(int id)
        {
            return Enclos_area_mapper.Enclos_to_EnclosAreaDisplay(Enclos_repo.GetOne_enclos(id));
        }

        public static List<Enclos_areaDisplay> Get_EnclosAreaDisplay()
        {
            List<Enclos> l_e = Enclos_repo.Get_enclos();

            List<Enclos_areaDisplay> lead = new List<Enclos_areaDisplay>();

            foreach (Enclos item in l_e)
            {
                lead.Add(Enclos_area_mapper.Enclos_to_EnclosAreaDisplay(item));
            }
            return lead;
        }

        public static List<Enclos_areaDisplay> GetFilter_EnclosAreaDisplay(string query, Dictionary<string, object> param)
        {
            List<Enclos> l_e = Enclos_repo.GetFilter_enclos(query, param);

            List<Enclos_areaDisplay> lead = new List<Enclos_areaDisplay>();

            foreach (Enclos item in l_e)
            {
                lead.Add(Enclos_area_mapper.Enclos_to_EnclosAreaDisplay(item));
            }
            return lead;
        }

        public static int Delete_EnclosAreaDisplay(int id)
        {
            return Enclos_repo.Delete_enclos(id);
        }
    }
}