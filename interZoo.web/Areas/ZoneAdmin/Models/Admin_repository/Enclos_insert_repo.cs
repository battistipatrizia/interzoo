﻿using interZoo.web.Areas.ZoneAdmin.Models.Mappers_area;
using Interzoo.Models.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace interZoo.web.Areas.ZoneAdmin.Models.Admin_repository
{
    public static class Enclos_insert_repo
    {

        public static int Insert_enclos_insert(Enclos_insert ei)
        {
            return Enclos_repo.Insert_enclos(Enclos_area_mapper.MapEnclosInsert_to_Enclos(ei));
        }

        public static Enclos_insert GetOne_EnclosInsert(int id)
        {
            return Enclos_area_mapper.MapEnclos_to_EnclosInsert(Enclos_repo.GetOne_enclos(id));
        }

        public static int Update_EnclosInsert(Enclos_insert ei)
        {
            return Enclos_repo.Update_enclos(Enclos_area_mapper.MapEnclosInsert_to_Enclos(ei));
        }
    }
}