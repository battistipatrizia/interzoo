﻿using interZoo.web.Areas.ZoneAdmin.Models.Mappers_area;
using Interzoo.Models;
using Interzoo.Models.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace interZoo.web.Areas.ZoneAdmin.Models.Admin_repository
{
    public static class Environnement_areaDisplay_repo
    {
        public static List<Environnement_areaDisplay> Get_envi_areaDisplay()
        {
            List<Environnement> le = Environnement_repo.Get_envi();

            List<Environnement_areaDisplay> liste_envi_areaDisplay = new List<Environnement_areaDisplay>();

            foreach (Environnement item in le)
            {
                Environnement_areaDisplay ea = Envi_area_mapper.MapEnvi_to_EnviAreaDisplay(item);

                liste_envi_areaDisplay.Add(ea);
            }

            return liste_envi_areaDisplay;
        }

        public static Environnement_areaDisplay GetOne_envi_areaDisplay(int id)
        {
            return Envi_area_mapper.MapEnvi_to_EnviAreaDisplay(Environnement_repo.getOne_envi(id));
        }
    }
}