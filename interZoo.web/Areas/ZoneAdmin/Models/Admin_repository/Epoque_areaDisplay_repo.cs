﻿using interZoo.web.Areas.ZoneAdmin.Models.Mappers_area;
using Interzoo.Models;
using Interzoo.Models.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace interZoo.web.Areas.ZoneAdmin.Models.Admin_repository
{
    public class Epoque_areaDisplay_repo
    {
        public static List<Epoque_areaDisplay> Get_epoque_areaDisplay()
        {
            List<Epoque> le = Epoque_repo.Get_epoque();

            List<Epoque_areaDisplay> liste_epoque_areaDisplay = new List<Epoque_areaDisplay>();

            foreach (Epoque item in le)
            {
                Epoque_areaDisplay ea = Epoque_area_mapper.MapEpoque_to_EpoqueAreaDisplay(item);

                liste_epoque_areaDisplay.Add(ea);
            }

            return liste_epoque_areaDisplay;
        }


        public static Epoque_areaDisplay GetOne_epoque_areaDisplay(int id)
        {
            return Epoque_area_mapper.MapEpoque_to_EpoqueAreaDisplay(Epoque_repo.getOne_epoque(id));
        }
    }
}