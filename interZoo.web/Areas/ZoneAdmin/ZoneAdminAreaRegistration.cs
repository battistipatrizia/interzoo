﻿using System.Web.Mvc;

namespace interZoo.web.Areas.ZoneAdmin
{
    public class ZoneAdminAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "ZoneAdmin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "ZoneAdmin_default",
                "ZoneAdmin/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}