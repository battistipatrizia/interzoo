﻿using interZoo.web.Areas.ZoneAdmin.Models;
using interZoo.web.Areas.ZoneAdmin.Models.Admin_repository;
using interZoo.web.Areas.ZoneAdmin.Models.Mappers_area;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace interZoo.web.Areas.ZoneAdmin.Controllers
{
    public class EnclosController : Controller
    {
        // GET: ZoneAdmin/Enclos
        public ActionResult Choix_e()  //les noms d'Actions ne sont pas sensés avoir d'_ en vrai...
        {
            return View();
        }

        public ActionResult Recherche_e()
        {
            Enclos_rencherche_e eee = new Enclos_rencherche_e();
            return View(eee);
        }

        [HttpPost]
        public ActionResult Recherche_e(Enclos_search es)
        {
            List<Enclos_areaDisplay> lead = new List<Enclos_areaDisplay>();
            if (es.Query_where != "")
            {
                lead = Enclos_areaDisplay_repo.GetFilter_EnclosAreaDisplay(es.Query_where, es.Parametres_where);

            }
            else
            { 
                lead = Enclos_areaDisplay_repo.Get_EnclosAreaDisplay();
            }

            return View("Affichage_e", lead);
        }

        public ActionResult Affichage_e()
        {
            List<Enclos_areaDisplay> lead = Enclos_areaDisplay_repo.Get_EnclosAreaDisplay();
            return View();
        }

        [HttpPost]
        public ActionResult Affichage_e(int Id_enclos)
        {
            if (Id_enclos > 0) 
            {
                return RedirectToAction("Update_e", "Enclos", new { id = Id_enclos }); 

            }
            ViewBag.Message = "ERREUR: IMPOSSIBLE D'ATTEINDRE LA REFERENCE";

            return View();
        }

        public ActionResult Ajouter_e()
        {
            Recherche_e_display red = new Recherche_e_display();
            return View(red);
        }

        [HttpPost]
        public ActionResult Ajouter_e(Enclos_insert ei)
        {
            ei.Id_enclos = Enclos_insert_repo.Insert_enclos_insert(ei);
            ////recrer modele pour update!
            if (ei.Id_enclos > 0) //insert nous renvoie le numero d'ID de ce qu'on vient d'inséré. Si il est plus petit que 0, c'est que -1 à été renvoyé  --> con            {
            {    return RedirectToAction("Update_e", "Enclos", new { id = ei.Id_enclos });  //pour passer des variables dans le redirectToAction (on le fait sous forme d'un abjet (anonyme ici)

            }
            ViewBag.Message = "ERREUR LORS DE L'INSERTION.";

            return View();
        }

        public ActionResult Update_e(int id)
        {
            Update_e_EnclosDisplay_general ueed = new Update_e_EnclosDisplay_general(Enclos_areaDisplay_repo.GetOne_EnclosAreaDisplay(id));
            return View(ueed);
        }

        [HttpPost]
        public ActionResult Update_e(Enclos_insert ei)
        {
            int test = Enclos_insert_repo.Update_EnclosInsert(ei);
            if(ei.Id_enclos > 0)
            {
                ViewBag.Message = "Update effectuée";
                Update_e_EnclosDisplay_general ueed = new Update_e_EnclosDisplay_general(Enclos_areaDisplay_repo.GetOne_EnclosAreaDisplay(ei.Id_enclos));
                return View(ueed);
            }

            ViewBag.Message = "Problème lors de la mise à jour. Update non effectuée.";

            return View("choix_e");


        }

        public ActionResult Delete_e(int Id_enclos)
        {
            if (Enclos_areaDisplay_repo.Delete_EnclosAreaDisplay(Id_enclos) > 0)
            {
                ViewBag.Message = "Enclos supprimé";
            }
            else
            {
                ViewBag.Message = "Problème lors de la suppression. Enclos non supprimé";
            }

            return View("choix_e");
        }

    }
}