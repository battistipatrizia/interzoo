﻿using System.Web;
using System.Web.Optimization;

namespace interZoo.web
{
    public class BundleConfig
    {

        //AspNetHostingPermissionLevel bundle est un objet c#qui contient les liens vers le css et js

        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                       "~/Scripts/datepicker/jquery-ui.css",
                       "~/Scripts/datepicker/jquery-ui.min.js",
                       "~/Scripts/datepicker_init.js"
                       ));


            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(  //bundle pour les scripts
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/js").Include(
                     "~/Scripts/js/css3-mediaqueries.js",
                     "~/Scripts/js/html5.js",
                     "~/Scripts/js/jquery1111.min.js",
                     "~/Scripts/js/lightbox-plus-jquery.min.js",
                     "~/Scripts/js/script.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(         //content pour les styles
                      "~/Content/bootstrap.css",
                      //"~/Content/site.css",
                      "~/Content/css/lightbox.css",
                      "~/Content/css/menu.css",
                      "~/Content/css/style.css",
                      "~/Content/css/zerogrid.css",
                      "~/Content/petitepatte.css",
                      "~/Content/css/tableau.css"
                      ));

            bundles.Add(new StyleBundle("~/Content/dinos").Include(         //content pour les styles
                       "~/Content/bootstrap.css",
                       //"~/Content/css/lightbox.css",
                       //"~/Content/css/menu.css",
                       //"~/Content/css/style.css",
                       //"~/Content/css/zerogrid.css",
                       "~/Content/css/nosDinos.css"


                       ));

        }
    }
}
