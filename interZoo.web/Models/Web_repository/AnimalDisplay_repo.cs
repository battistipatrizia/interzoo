﻿using interZoo.web.Models.Mappers;
using Interzoo.Models;
using Interzoo.Models.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace interZoo.web.Models.Web_repository
{
    public class AnimalDisplay_repo
    {
        public static List<AnimalDisplay> Get_animal_display() //permet de reprendre les infos (Get) de la DB en faisant appel la methode Get_gift mais en renvoyant un object List<Gift_display> adapté au traitement dans le (Home) controller et action (Index) adéquoits
        {
            List<Animal> la = Animal_repo.Get_animal();

            List<AnimalDisplay> liste_animal_display = new List<AnimalDisplay>();

            foreach (Animal item in la)
            {
                AnimalDisplay ad = AnimalMapper.MapperToAnimalDisplay(item);

                liste_animal_display.Add(ad);
            }

            return liste_animal_display;
        }

        public static AnimalDisplay GetOne_animal_display(int id) //permet de reprendre les infos (Get) de la DB en faisant appel la methode Get_gift mais en renvoyant un object List<Gift_display> adapté au traitement dans le (Home) controller et action (Index) adéquoits
        {
            Animal a = Animal_repo.GetOne_animal(id);

            List<AnimalDisplay> liste_animal_display = new List<AnimalDisplay>();

           
            AnimalDisplay ad = AnimalMapper.MapperToAnimalDisplay (a);


            return ad;
        }

        public static List<AnimalDisplay> GetFilter_animal_display(string query_where, Dictionary<string, object> parametres) //permet de reprendre les infos (Get) de la DB en faisant appel la methode Get_gift mais en renvoyant un object List<Gift_display> adapté au traitement dans le (Home) controller et action (Index) adéquoits
        {
            List<Animal> la = Animal_repo.GetFilter_animal(query_where, parametres);

            List<AnimalDisplay> liste_animal_display = new List<AnimalDisplay>();


            foreach (Animal item in la)
            {
                AnimalDisplay a = AnimalMapper.MapperToAnimalDisplay(item);

                liste_animal_display.Add(a);
            }

            return liste_animal_display;
        }

    }
}