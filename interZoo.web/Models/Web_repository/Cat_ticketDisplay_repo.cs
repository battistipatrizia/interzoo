﻿using Interzoo.Models.ModelBilleterie;
using Interzoo.Models.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace interZoo.web.Models.Web_repository
{
    public class Cat_ticketDisplay_repo
    {
        public static List<Cat_TicketDisplay> Get_ticket_display() 
        {
            List<Categorie_ticket> la =Categorie_ticket_repo.Get();

            List<Cat_TicketDisplay> liste_ticket_display = new List<Cat_TicketDisplay>();

            foreach (Categorie_ticket item in la)
            {
                Cat_TicketDisplay ad = new Cat_TicketDisplay
                {
                    Id_cat=item.Id_cat,
                    Nom_cat = item.Nom_cat,
                    Prix_ticket=item.Prix_ticket,
                    Details=item.Details,
                    Prix_abo=item.Prix_abo

                };

                liste_ticket_display.Add(ad);
            }

            return liste_ticket_display;
        }
    }
}