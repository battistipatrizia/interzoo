﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace interZoo.web.Models
{
    public class AnimalDisplay
    {
        //necessaire?///
        //private int _id_animal_display;
        //necessaire?///
        private string _nom;
        private string _picture;
        private string _espece;
        //private DateTime _date_naiss;
        private double _age_annees;
        private double _age_jours;
        private string _genre;
        private string _description;

      //necessaire?///
        //public int Id_animal_display
        //{
        //    get
        //    {
        //        return _id_animal_display;
        //    }

        //    set
        //    {
        //        _id_animal_display = value;
        //    }
        ////}
        //necessaire?///

        public string Nom
        {
            get
            {
                return _nom;
            }

            set
            {
                _nom = value;
            }
        }

        public string Picture
        {
            get
            {
                return _picture;
            }

            set
            {
                _picture = value;
            }
        }

        public string Espece
        {
            get
            {
                return _espece;
            }

            set
            {
                _espece = value;
            }
        }

        //public DateTime Date_naiss
        //{
        //    get
        //    {
        //        return _date_naiss;
        //    }

        //    set
        //    {
        //        _date_naiss = value;
        //    }
        ////}

        public double Age_annees
        {
            get
            {
                return _age_annees;
            }

            set
            {
                _age_annees = value;
            }
        }

        public double Age_jours
        {
            get
            {
                return _age_jours;
            }

            set
            {
                _age_jours = value;
            }
        }

        public string Genre
        {
            get
            {
                return _genre;
            }

            set
            {
                _genre = value;
            }
        }

        public string Description
        {
            get
            {
                return _description;
            }

            set
            {
                _description = value;
            }
        }
    }
}