﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace interZoo.web.Models
{
    public class Cat_TicketDisplay
    {
        private int id_cat;
        private string nom_cat;
        private double prix_ticket;
        private double prix_abo;
        private string details;

        public string Nom_cat
        {
            get
            {
                return nom_cat;
            }

            set
            {
                nom_cat = value;
            }
        }

        public double Prix_ticket
        {
            get
            {
                return prix_ticket;
            }

            set
            {
                prix_ticket = value;
            }
        }

        public string Details
        {
            get
            {
                return details;
            }

            set
            {
                details = value;
            }
        }

        public double Prix_abo
        {
            get
            {
                return prix_abo;
            }

            set
            {
                prix_abo = value;
            }
        }

        public int Id_cat
        {
            get
            {
                return id_cat;
            }

            set
            {
                id_cat = value;
            }
        }
    }
}