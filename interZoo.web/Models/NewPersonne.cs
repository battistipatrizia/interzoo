﻿using Interzoo.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace interZoo.web.Models
{
    public class NewPersonne
    {
        private int _idPersonne;
        private string _nom,_prenom,_codepostal,_loc,_login,_pwd;
        private DateTime? _datenaiss;
        private bool _isActif, _isAdmin;

        public int IdPersonne
        {
            get { return _idPersonne; }
            set { _idPersonne = value; }
        }
        public  string Nom
        {
            get { return _nom; }
            set { _nom=value; }
        }
        public string Prenom
        {
            get { return _prenom; }
            set { _prenom=value; }
        }
        public DateTime? DateNaiss
        {
            get { return _datenaiss; }
            set { _datenaiss = value; }
        }
        public string CodePostal
        {
            get { return _codepostal; }
            set { _codepostal = value; }
        }
        public string Localite
        {
            get { return _loc; }
            set { _loc = value; }
        }
        public string Login
        {
            get { return _login; }
            set { _login = value; }
        }
        public string Pwd
        {
            get { return _pwd; }
            set { _pwd = value; }
        }
        public bool IsActif
        {
            get { return _isActif; }
            set { _isActif = value; }
        }
        public bool IsAdmin
        {
            get { return _isAdmin; }
            set { _isAdmin = value; }
        }


        public int insert()/*commentaire pour marie-ange*/
        {
            string query = @"EXECUTE [dbo].[CreateUser] @Nom, @Prenom, @DateNaiss, @CodePostal, @Localite, @Login, @Pwd";

            Dictionary<string, object> parametres = new Dictionary<string, object>();
            parametres.Add("Nom",Nom);
            parametres.Add("Prenom", Prenom);
            parametres.Add("DateNaiss", DateNaiss);
            parametres.Add("CodePostal", CodePostal);
            parametres.Add("Localite", Localite);
            parametres.Add("Login", Login);
            parametres.Add("Pwd", Pwd);

            DBConnect Db = new DBConnect();
            return Db.Insert(parametres, query);
        }
    }
}