﻿using Interzoo.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace interZoo.web.Models
{
    public class Login:NewPersonne
    {
        //private int idPersonne;
        private string _pseudo;

        //public int IdPersonne
        //{
        //    get
        //    {
        //        return idPersonne;
        //    }

        //    set
        //    {
        //        idPersonne = value;
        //    }
        //}

        public string Pseudo
        {
            get
            {
                return _pseudo;
            }
            set
            {
                _pseudo = value;
            }
        }

        //public string Pwd
        //{
        //    get
        //    {
        //        return _pwd;
        //    }
        //    set
        //    {
        //        _pwd = value;
        //    }
        //}


        public  bool check()
        {
            string query = "SELECT * FROM Users WHERE Login=@Login and Pwd =@Pwd";

            Dictionary<string, Object> parametres = new Dictionary<string, Object>();
            parametres.Add("Login", Pseudo);
            parametres.Add("Pwd", Pwd);
            //parametres.Add("Nom", Nom);

            DBConnect Db = new DBConnect();
            List<Dictionary<string, Object>> retour = Db.getFilter(query, parametres);
            this.IdPersonne = (int) retour[0]["IdPersonne"]; //pour ajouter l'id de la personne dans le login sans devoir le remettre dans la query
           // this.Nom = retour[0]["Nom"].ToString();

            return retour.Count() > 0;
        }
    }
}