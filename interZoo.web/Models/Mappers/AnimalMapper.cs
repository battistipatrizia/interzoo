﻿using Interzoo.DAL;
using Interzoo.Models;
using Interzoo.Models.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace interZoo.web.Models.Mappers
{
    public static class AnimalMapper
    {

        public static AnimalDisplay MapperToAnimalDisplay(Animal a)
        {
            TimeSpan span = DateTime.Now - a.Date_naiss;
            double annees = Math.Floor(span.TotalDays / 365);
            double jours = Math.Floor(span.TotalDays - (annees * 365));

            Espece e = Espece_repo.GetOne_espece(a.Id_espece);

            DBConnect db = new DBConnect();

            string cette_espece = Espece_repo.GetOne_espece(a.Id_espece).Nom_espece;

            return new AnimalDisplay
            {
                //Id_animal_display = a.ID_animal,
                Nom = a.Nom,
                Age_annees = annees,
                Age_jours = jours,
                Picture = a.Picture,
                Genre = a.Genre,
                Description = a.Description,
                Espece = cette_espece
            };
        }
    }
}