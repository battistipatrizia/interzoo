$(document).ready(function(){
    $('#date').datepicker({
        firstDay: 1, 
        dayNamesMin: [ "Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa" ],
        monthNames: [ "Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre" ],
        dateFormat: "yy/mm/dd",
        showOtherMonths: true,
        minDate: new Date(),
        maxDate: "+15d",
        beforeShowDay: $.datepicker.noWeekends
    });

})