﻿using interZoo.web.Areas.UserLog.Models;
using interZoo.web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace interZoo.web.Tools
{
    public static class SessionUtils
    {
        public static Login ConnectedUser
        {
            get
            {
                return (Login)
                    HttpContext.Current.Session["ConnectedUser"];
            }

            set
            {
                HttpContext.Current.Session["ConnectedUser"] = value;
         

            }
        }

        public static  FormModel InfoReservation
        {
            get {
                return (FormModel)
                    HttpContext.Current.Session["InfoReservation"];
            }
            set
            {
                HttpContext.Current.Session["InfoReservation"] = value;
            }
        }

        public static List<CreateTicket> ListeDesTickets
        {
            get
            {
                return (List<CreateTicket>)
                    HttpContext.Current.Session["ListeDesTickets"];
            }
            set
            {
                HttpContext.Current.Session["ListeDesTickets"] = value;
            }
        }
        public static CreateReservation UneReservation
        {
            get
            {
                return (CreateReservation)
                    HttpContext.Current.Session["UneReservation"];
            }
            set
            {
                HttpContext.Current.Session["UneReservation"] = value;
            }
        }
    }
}