﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interzoo.DAL
{
    public interface IDBConnect
    {
        List<Dictionary<string, object>> get(string Nomtable);

        Dictionary<string, object> getOne(int Id, string Nomtable, string nom_colonne);

        List<Dictionary<string, object>> getFilter(string query, Dictionary<string, object> parametres);

        int Insert(Dictionary<string, object> d, string query);

        int Update(Dictionary<string, object> d, string query);

        int Delete(int Id, string Nomtable, string NomId);
    }
}
