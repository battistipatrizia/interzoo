﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interzoo.Models
{
    public class Animal
    {
        private int _iD_animal;
        private string _nom;
        private string _picture;
        private DateTime _date_naiss;
        private string _genre;
        private string _description;
        private int _id_espece;
        private int _id_enclos;

        public Animal() { }

        public Animal(string nom, string picture, DateTime date_naiss, string genre, string description, int id_espece, int id_enclos)
        {
            this._nom = nom;
            this._picture = picture;
            this._date_naiss = date_naiss;
            this._genre = genre;
            this._description = description;
            this._id_espece = id_espece;
            this._id_enclos = id_enclos;
        }

        public int ID_animal
        {
            get
            {
                return _iD_animal;
            }

            set
            {
                _iD_animal = value;
            }
        }

        public string Nom
        {
            get
            {
                return _nom;
            }

            set
            {
                _nom = value;
            }
        }

        public string Picture
        {
            get
            {
                return _picture;
            }

            set
            {
                _picture = value;
            }
        }

        public DateTime Date_naiss
        {
            get
            {
                return _date_naiss;
            }

            set
            {
                _date_naiss = value;
            }
        }

        public string Genre
        {
            get
            {
                return _genre;
            }

            set
            {
                _genre = value;
            }
        }

        public string Description
        {
            get
            {
                return _description;
            }

            set
            {
                _description = value;
            }
        }

        public int Id_espece
        {
            get
            {
                return _id_espece;
            }

            set
            {
                _id_espece = value;
            }
        }

        public int Id_enclos
        {
            get
            {
                return _id_enclos;
            }

            set
            {
                _id_enclos = value;
            }
        }
    }
}
