﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interzoo.Models
{
    public class Espece
    {
        private int _id_espece;
        private string _nom_espece;
        private double _coefficient;
        private int _id_regime;
        private int _id_envi;
        private int _id_epoque;

        public Espece()
        { }

        public Espece(int id_espece, string nom_espece, double coefficient, int id_regime, int id_envi, int id_epoque)
        {
            this._id_espece = id_espece;
            this._nom_espece = nom_espece;
            this._coefficient = coefficient;
            this._id_regime = id_regime;
            this._id_envi = id_envi;
            this._id_epoque = id_epoque;
        }

        public int Id_espece
        {
            get
            {
                return _id_espece;
            }

            set
            {
                _id_espece = value;
            }
        }

        public string Nom_espece
        {
            get
            {
                return _nom_espece;
            }

            set
            {
                _nom_espece = value;
            }
        }

        public double Coefficient
        {
            get
            {
                return _coefficient;
            }

            set
            {
                _coefficient = value;
            }
        }

        public int Id_regime
        {
            get
            {
                return _id_regime;
            }

            set
            {
                _id_regime = value;
            }
        }

        public int Id_envi
        {
            get
            {
                return _id_envi;
            }

            set
            {
                _id_envi = value;
            }
        }

        public int Id_epoque
        {
            get
            {
                return _id_epoque;
            }

            set
            {
                _id_epoque = value;
            }
        }
    }
}
