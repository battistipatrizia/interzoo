﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interzoo.Models
{
     public class Visiteur: Personne
    {
        private double _prix;
        public int _IdPersonne;
        public int _IdVisiteur;

        public Visiteur()
        {

        }

        public Visiteur(string nom, string prenom, DateTime date_naiss) : base(nom, prenom, date_naiss)
        {
            Prix = 30;
        }

        public double Prix
        {
            get { return _prix; }
            set { _prix = value; }
        }
        public int idPersonne
        {
            get { return _IdPersonne; }
            set {_IdPersonne = value; }
        }
        public int idVisiteur
        {
            get { return _IdVisiteur; }
            set { _IdVisiteur = value; }
        }
    }
}
