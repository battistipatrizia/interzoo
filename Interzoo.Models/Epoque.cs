﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interzoo.Models
{
    public class Epoque
    {
        private int _id_epoque;
        private string _nom_epoque;
        private string _details_epoque;

        public Epoque()
        { }


        public Epoque(int id_epoque, string nom_epoque, string details_epoque)
        {
            this.Id_epoque = id_epoque;
            this.Nom_epoque = nom_epoque;
            this.Details_epoque = details_epoque;
        }

        public int Id_epoque
        {
            get
            {
                return _id_epoque;
            }

            set
            {
                _id_epoque = value;
            }
        }

        public string Nom_epoque
        {
            get
            {
                return _nom_epoque;
            }

            set
            {
                _nom_epoque = value;
            }
        }

        public string Details_epoque
        {
            get
            {
                return _details_epoque;
            }

            set
            {
                _details_epoque = value;
            }
        }
    }
}
