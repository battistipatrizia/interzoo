﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interzoo.Models
{
    public class Personne
    {
        private string _nom;
        private string _prenom;
        private string _localite;
        private int _code_postal;
        private DateTime _date_naissance;

        public Personne()
        {
            
        }

        public Personne(string nom, string prenom, DateTime date_naiss)
        {
            Nom = nom;
            Prenom = prenom;
            Date_naissance = date_naiss;
        }


        //public Personne(string nom, string prenom, DateTime date_naiss, int code_postal)
        //{
        //    Nom = nom;
        //    Prenom = prenom;
        //    Date_naissance = date_naiss;
        //    Code_postal = code_postal;
        //}

        //public Personne(string nom, string prenom, DateTime date_naiss, int code_postal, string localite)
        //{
        //    Nom = nom;
        //    Prenom = prenom;
        //    Date_naissance = date_naiss;
        //    Code_postal = code_postal;
        //    Localite = localite;
        //}

        public string Nom
        {
            get { return _nom; }
            set { _nom = value; }
        }

        public string Prenom
        {
            get { return _prenom; }
            protected set { _prenom = value; }
        }

        public int Code_postal
        {
            get { return _code_postal; }
            protected set { _code_postal = value; }
        }

        public string Localite
        {
            get { return _localite; }
            protected set { _localite = value; }
        }

        public DateTime Date_naissance
        {
            get { return _date_naissance; }
            protected set { _date_naissance = value; }
        }

    }
}
