﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interzoo.Models
{
    public class Soigneur : Personnel
    {
        public Soigneur(string nom, string prenom, DateTime date_naiss, double salaire, int anciennete, string coordonnees, string matricule) : base(nom, prenom, date_naiss, salaire, anciennete, coordonnees, matricule)
        {
        }
    }
}
