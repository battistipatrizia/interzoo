﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interzoo.Models
{
    public class Enclos
    {
        private int _id_enclos;
        private double _taille;
        private double _capacite_accueil;
        private int _id_epoque;
        private int _id_envi;
        private string _details_enclos;

        public Enclos()
        { }

        public Enclos(int id_enclos, double taille, double capacite_accueil, int id_epoque, int id_envi, string details_enclos)
        {
            this._id_enclos = id_enclos;
            this._taille = taille;
            this._capacite_accueil = capacite_accueil;
            this._id_epoque = id_epoque;
            this._id_envi = id_envi;
            this._details_enclos = details_enclos;
        }

        public int Id_enclos
        {
            get
            {
                return _id_enclos;
            }

            set
            {
                _id_enclos = value;
            }
        }

        public double Taille
        {
            get
            {
                return _taille;
            }

            set
            {
                _taille = value;
            }
        }

        public double Capacite_accueil
        {
            get
            {
                return _capacite_accueil;
            }

            set
            {
                _capacite_accueil = value;
            }
        }

        public int Id_epoque
        {
            get
            {
                return _id_epoque;
            }

            set
            {
                _id_epoque = value;
            }
        }

        public int Id_envi
        {
            get
            {
                return _id_envi;
            }

            set
            {
                _id_envi = value;
            }
        }

        public string Details_enclos
        {
            get
            {
                return _details_enclos;
            }

            set
            {
                _details_enclos = value;
            }
        }
    }
}
