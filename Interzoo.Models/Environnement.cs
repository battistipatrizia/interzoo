﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interzoo.Models
{
    public class Environnement
    {

        private int _id_envi;
        private string _type_envi;
        private string _description_envi;


        public Environnement()
        { }

        public Environnement(int id_envi, string type_envi, string description_envi)
        {
            this._id_envi = id_envi;
            this._type_envi = type_envi;
            this._description_envi = description_envi;
        }

        public int Id_envi
        {
            get
            {
                return _id_envi;
            }

            set
            {
                _id_envi = value;
            }
        }

        public string Type_envi
        {
            get
            {
                return _type_envi;
            }

            set
            {
                _type_envi = value;
            }
        }

        public string Description_envi
        {
            get
            {
                return _description_envi;
            }

            set
            {
                _description_envi = value;
            }
        }
    }
}
