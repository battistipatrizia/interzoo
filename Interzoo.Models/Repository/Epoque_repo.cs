﻿using Interzoo.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interzoo.Models.Repository
{
    public class Epoque_repo
    {
        public static Epoque getOne_epoque(int id)
        {
            DBConnect dbtest = new DBConnect();

            Dictionary<string, object> e_brut = dbtest.getOne(id, "Epoque", "Id_epoque");

            Epoque e = new Epoque();

            e.Id_epoque = (int)e_brut["Id_epoque"];
            e.Nom_epoque = e_brut["Nom_epoque"].ToString();
            e.Details_epoque = e_brut["Details_epoque"] == DBNull.Value ? null : e_brut["Details_epoque"].ToString();

            return e;
        }

        public static List<Epoque> Get_epoque()
        {
            DBConnect dbtest = new DBConnect();

            List<Dictionary<string, object>> liste_e_brut = dbtest.get("Epoque");

            List<Epoque> liste_e = new List<Epoque>();


            foreach (var item in liste_e_brut)
            {
                Epoque e = new Epoque();

                e.Id_epoque = (int)item["Id_epoque"];
                e.Nom_epoque = item["Nom_epoque"].ToString();
                e.Details_epoque = item["Details_epoque"] == DBNull.Value ? null : item["Details_epoque"].ToString();

                liste_e.Add(e);

            }
            return liste_e;
        }

        public static List<Epoque> GetFilter_epoque(string query_where, Dictionary<string, object> parametres)
        {
            DBConnect dbtest = new DBConnect();
            string query_filter = "SELECT * FROM Epoque WHERE " + query_where;

            List<Dictionary<string, Object>> liste_e_filter_brute = dbtest.getFilter(query_filter, parametres);

            List<Epoque> liste_e = new List<Epoque>();

            foreach (var item in liste_e_filter_brute)
            {
                Epoque e = new Epoque();

                e.Id_epoque = (int)item["Id_epoque"];
                e.Nom_epoque = item["Nom_epoque"].ToString();
                e.Details_epoque = item["Details_epoque"] == DBNull.Value ? null : item["Details_epoque"].ToString();

                liste_e.Add(e);
            }

            return liste_e;
        }
    }
}
