﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using Interzoo.Models;

namespace Interzoo.Models
{
    public class AccessPersonne
    {

        SqlCommand _oCmd; //object commande
        SqlConnection _oConn; //objet connection


        public void getOne(int id)
        {
            //ConsoleKeyInfo disponible sure le site connection strings  --> trouver la bonne version
            //pour trouver le nom du serveur, aller dans sql manager, en haut de l'arborescence (WAD-11...) -> clic droit -> properties : name'
            string ConnectionString = @"Server = WAD-12\SQLEXPRESS; Database = InterZooDB; User Id = sa;
            Password = wad;";

            //création de l'objet de connection
            _oConn = new SqlConnection(ConnectionString);

            //ouverture de la connection
            _oConn.Open();
            _oCmd = new SqlCommand("Select * FROM Personne", _oConn);

            //équivaut aux deux lignes suivantes:
            //_oCmd.CommandText = "Select * FROM Personne";
            //_oCmd.Connection = _oConn;

            SqlDataReader Odr = _oCmd.ExecuteReader();

            while (Odr.Read())
            {
                string Nom = Odr["Nom"].ToString();

                //on doit faire gaffe car DateNaiss et autres peuvent être null et ne pourront donc être castés (null pour DB = DBNull)
                DateTime? DtNaiss = null;
                if (Odr["DateNaiss"] != DBNull.Value)
                {
                    DtNaiss = (DateTime)Odr["DateNaiss"];

                }
             
            }
            //si quand on a fini avec le DataReader on ne le ferme pas on ne saura plus rien faire d'autre (pas d'iNSERT, de DELETE,... => il faut penser à le fermer (!reflex);
            Odr.Close();



            //fermeture de la connection, pas obligatoire mais vaut mieux pour que se soit propre
            _oConn.Close();
        }



        public bool Insert(Personne P)
        {
            string ConnectionString = @"Server = WAD-12\SQLEXPRESS; Database = InterZooDB; User Id = sa;
            Password = wad;";

            _oConn = new SqlConnection(ConnectionString); //objet connection

            _oConn.Open();

            _oCmd = new SqlCommand(); //objet commande
            _oCmd.Connection = _oConn;

            //ajout Commande Texte

            // dans une chaîne de texte, l'@ permet qu'il comprenne que la string continue sur plusieurs lignes (sans concaténer)
            string requete = @"INSERT INTO [dbo].[Personne]
                               ([Nom]
                               ,[Prenom]
                               ,[DateNaiss]
                               ,[CodePostal]
                               ,[Localite])
                         VALUES
                               ('Jones'
                               ,'Hank'
                               ,null
                               ,1200
                               ,'Bruxelles')";

            _oCmd.CommandText = requete;

            _oCmd.ExecuteNonQuery(); // exécution de la requête (dépend laquelle on a besoin)
      
            _oConn.Close(); //fermeture connection

            //retourner true si tout est ok
            return false;
        }



        public bool Update(Personne P)
        {
            string ConnectionString = @"Server = WAD-11\SQLEXPRESS; Database = InterZooDB; User Id = sa;
            Password = wad;";

            _oConn = new SqlConnection(ConnectionString);

            _oConn.Open();
            _oCmd = new SqlCommand(); //objet commande
            _oCmd.Connection = _oConn;

            string requete = @"UPDATE [dbo].[Personne]
                               SET [Nom] = 'Lala'
                                  ,[Prenom] = 'Tubbies'
                                  ,[DateNaiss] = null
                                  ,[CodePostal] = 2000
                                  ,[Localite] = 'Loin'
                                WHERE [Nom] ='" + P.Nom + "'";

            _oCmd.CommandText = requete;

            _oCmd.ExecuteNonQuery();

            _oConn.Close();

            return false;

        }



        public bool Delete(Personne P)
        {
            string ConnectionString = @"Server = WAD-11\SQLEXPRESS; Database = InterZooDB; User Id = sa;
            Password = wad;";

            _oConn = new SqlConnection(ConnectionString);

            _oConn.Open();

            _oCmd = new SqlCommand(); //objet commande
            _oCmd.Connection = _oConn;

            string requete = @"DELETE FROM [dbo].[Personne]
                                WHERE [Nom] ='" + P.Nom + "'";

            _oCmd.CommandText = requete;

            _oCmd.ExecuteNonQuery(); 

            _oConn.Close();

            return false;
        }
    

    }
}
