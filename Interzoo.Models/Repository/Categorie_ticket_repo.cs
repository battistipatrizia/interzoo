﻿using Interzoo.DAL;
using Interzoo.Models.ModelBilleterie;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interzoo.Models.Repository
{
    public static class Categorie_ticket_repo

    {
        public static List<Categorie_ticket> Get()
        {
            DBConnect dbtest = new DBConnect();

            List<Dictionary<string, object>> liste_categorie_brut = dbtest.get("Categorie");

            List<Categorie_ticket> liste_cat = new List<Categorie_ticket>();


            foreach (var item in liste_categorie_brut)
            {
                Categorie_ticket c = new Categorie_ticket();
                c.Id_cat = (int)item["Id_cat"];
                c.Nom_cat = item["Nom_cat"].ToString();
                c.Prix_ticket = (double)item["Prix_ticket"];
                c.Prix_abo = (double)item["Prix_abo"];
                c.Details = item["Details"] == DBNull.Value ? null : item["Details"].ToString();

                liste_cat.Add(c);
            }

            return liste_cat;
        }

    }
}
