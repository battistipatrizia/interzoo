﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interzoo.Models;
using System.Diagnostics;

namespace Interzoo.DAL
{
    public class AccessVisiteur
    {
        SqlCommand _oCmd; //object commande
        SqlConnection _oConn; //objet connection
        string ConnectionString = @"Server = WAD-12\SQLEXPRESS; Database = InterZooDB; User Id = sa;
            Password = wad;";

        private bool Connect()  //on renvoie quelque chose pour ne pas la faire planter
        {
                                //création de l'objet de connection
            _oConn = new SqlConnection(ConnectionString);

                                //ouverture de la connection avec récupération de l'exception avec un try/catch
            try
            {
                _oConn.Open();
                _oCmd = new SqlCommand();
                _oCmd.Connection = _oConn;
                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return false;
            }

        }

        private bool Disconnect()
        {

            try
            {
                _oConn.Close(); // pour entourer d'un code un truc déjà créé : Ctrl k->s
                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return false;
            }
        }

        private bool Execute(string query)
        {
            if (Connect())
            {
                //associer la requête à la demande
                _oCmd.CommandText = query;
                //exécuter la requête
                _oCmd.ExecuteNonQuery();
                Disconnect();
                return true;
            }
            else
            {
                return false;
            }

        }

        public Visiteur getOne(int id)
        {
            Connect();

            _oCmd.CommandText=string.Format(@"Select * FROM Visiteur WHERE IdVisiteur={0}",id );


            SqlDataReader Odr = _oCmd.ExecuteReader();
            Visiteur vi = new Visiteur();

            while (Odr.Read())
            {
                if (Odr["Prix"] != null)
                {
                    vi.Prix = (float)Odr["Prix"]; // !!!si le champ peut contenir des nuls
                }

                //double d_prix = (double)Odr["Prix"];

                //int? IdPersonne = null;
                //if (Odr["IdPersonne"] != DBNull.Value)
                //{
                //    IdPersonne = (int)Odr["IdPersonne"];
                //}
            }

            Odr.Close();

            Disconnect();
            return vi;
        }

        public List<Visiteur> getAll()
        {
            Connect();

            _oCmd.CommandText = string.Format(@"Select * FROM Visiteur"); //On sélectionne tout


            SqlDataReader Odr = _oCmd.ExecuteReader();
            List <Visiteur> vi = new List<Visiteur>();//on crée une liste de visiteurs dans laquelle on va ajouter chaque instance dans le while

            while (Odr.Read())   //boucle pour remplir la liste
            {
                Visiteur Current = new Visiteur();
                Current.Prix= (double)Odr["Prix"];

                vi.Add(Current);
            }

            Odr.Close();

            Disconnect();

            return vi;    //on retourne la liste
        }

        public bool Insert(Visiteur Vi)
        {
            Connect();


            string requete =string.Format( @"INSERT INTO [dbo].[Visiteur]
                                   ([Prix]
                                   ,[IdPersonne])
                               VALUES
                                   ({0},{1})",Vi.Prix
                                   ,Vi.idPersonne);

            //_oCmd.CommandText = requete;

            //_oCmd.ExecuteNonQuery(); // exécution de la requête (dépend laquelle on a besoin)

            //Disconnect(); //fermeture connection

            Execute(requete);  // -> ligne qui remplace les trois précédentes car ces trois fct ont été mises dedans

                            //retourner true si tout est ok
            return true;
        }

        public bool Delete (int id)
        {
            Connect();
            string requete = @"DELETE FROM [dbo].[Visiteur]
                                WHERE [IdPersonne] ="+id+"";

            _oCmd.CommandText = requete;

            _oCmd.ExecuteNonQuery();

            Disconnect();

            return false;
        }

        public bool Update(Visiteur Vi)
        {
            Connect();

            string requete = string.Format(@"UPDATE [dbo].[Visiteur]
                               SET [Prix] = {0}, [IdPersonne]={1}             
                               WHERE [IdVisiteur]={2}",Vi.Prix,Vi.idPersonne,Vi.idVisiteur);

            _oCmd.CommandText = requete;

            _oCmd.ExecuteNonQuery();

            Disconnect();

            return true;
        }

    }
}
