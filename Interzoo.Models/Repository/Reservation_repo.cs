﻿using Interzoo.DAL;
using Interzoo.Models.ModelBilleterie;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interzoo.Models.Repository
{
    public static class Reservation_repo
    {
        public static int Insert_reservation(Reservation r)
        {
            DBConnect dbtest = new DBConnect();

            string query_insert = @"INSERT INTO [dbo].[Reservation] (Date_reservation, Nbr_ticket,IdPersonne) 
                                   OUTPUT INSERTED.Id_reservation VALUES (@Date_reservation, @Nbr_ticket,  @IdPersonne)";

            Dictionary<string, object> parametres_insert = new Dictionary<string, object>();

            parametres_insert.Add("@Date_reservation", r.Date_reservation); //le nom avec ou sans @, le programme comprends les deux apparemment: dans le execute, les entrées du tableau seront données (la Key en Key et la Value en Value) au SqlParameter (=Dictionnaire) qui servira à remplacer les @nom par les valeurs du tableau SqlParameter dont la clef @Nom ou Nom correspond
            parametres_insert.Add("@Nbr_ticket", r.Nbr_ticket);
            parametres_insert.Add("@IdPersonne", r.IdPersonne);


            return dbtest.Insert(parametres_insert, query_insert);
        }
    }
}
