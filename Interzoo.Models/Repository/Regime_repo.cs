﻿using Interzoo.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interzoo.Models.Repository
{
    class Regime_repo
    {
        public static Regime getOne_regime(int id)
        {
            DBConnect dbtest = new DBConnect();

            Dictionary<string, object> r_brut = dbtest.getOne(id, "Regime", "Id_regime");

            Regime r = new Regime();

            r.Id_regime = (int)r_brut["Id_regime"];
            r.Type_regime = r_brut["Type_regime"].ToString();
            r.Description_regime = r_brut["Description"] == DBNull.Value ? null : r_brut["Description_regime"].ToString();

            return r;
        }

        public static List<Regime> Get_regime()
        {
            DBConnect dbtest = new DBConnect();

            List<Dictionary<string, object>> liste_r_brut = dbtest.get("Regime");

            List<Regime> liste_r = new List<Regime>();


            foreach (var item in liste_r_brut)
            {
                Regime r = new Regime();

                r.Id_regime = (int)item["Id_regime"];
                r.Type_regime = item["Type_regime"].ToString();
                r.Description_regime = item["Description"] == DBNull.Value ? null : item["Description_regime"].ToString();

                liste_r.Add(r);

            }
            return liste_r;
        }

        public static List<Regime> GetFilter_regime(string query_where, Dictionary<string, object> parametres)
        {
            DBConnect dbtest = new DBConnect();
            string query_filter = "SELECT * FROM Regime WHERE " + query_where;

            List<Dictionary<string, Object>> liste_r_filter_brute = dbtest.getFilter(query_filter, parametres);

            List<Regime> liste_r = new List<Regime>();

            foreach (var item in liste_r_filter_brute)
            {
                Regime r = new Regime();

                r.Id_regime = (int)item["Id_regime"];
                r.Type_regime = item["Type_regime"].ToString();
                r.Description_regime = item["Description"] == DBNull.Value ? null : item["Description_regime"].ToString();

                liste_r.Add(r);
            }

            return liste_r;
        }
    }
}
