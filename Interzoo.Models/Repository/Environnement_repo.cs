﻿using Interzoo.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interzoo.Models.Repository
{
    public class Environnement_repo
    {
        public static Environnement getOne_envi(int id)
        {
            DBConnect dbtest = new DBConnect();

            Dictionary<string, object> e_brut = dbtest.getOne(id, "Environnement", "Id_envi");

            Environnement e = new Environnement();

            e.Id_envi = (int)e_brut["Id_envi"];
            e.Type_envi = e_brut["Type_envi"].ToString();
            e.Description_envi = e_brut["Description_envi"] == DBNull.Value ? null : e_brut["Description_envi"].ToString();

            return e;
        }

        public static List<Environnement> Get_envi()
        {
            DBConnect dbtest = new DBConnect();

            List<Dictionary<string, object>> liste_e_brut = dbtest.get("Environnement");

            List<Environnement> liste_e = new List<Environnement>();


            foreach (var item in liste_e_brut)
            {
                Environnement e = new Environnement();
                e.Id_envi = (int)item["Id_envi"];
                e.Type_envi = item["Type_envi"].ToString();
                e.Description_envi = item["Description_envi"] == DBNull.Value ? null : item["Description_envi"].ToString();

                liste_e.Add(e);

            }
                return liste_e;
        }

        public static List<Environnement> GetFilter_envi(string query_where, Dictionary<string, object> parametres)
        {
            DBConnect dbtest = new DBConnect();
            string query_filter = "SELECT * FROM Environnement WHERE " + query_where;

            List<Dictionary<string, Object>> liste_envi_filter_brute = dbtest.getFilter(query_filter, parametres);

            List<Environnement> liste_e = new List<Environnement>();

            foreach (var item in liste_envi_filter_brute)
            {
                Environnement e = new Environnement();
                e.Id_envi = (int)item["Id_envi"];
                e.Type_envi = item["Type_envi"].ToString();
                e.Description_envi = item["Description_envi"] == DBNull.Value ? null : item["Description_envi"].ToString();

                liste_e.Add(e);
            }

            return liste_e;
        }
    }
}
