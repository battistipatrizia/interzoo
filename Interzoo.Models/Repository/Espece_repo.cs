﻿using Interzoo.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interzoo.Models.Repository
{
    public class Espece_repo
    {
        public static Espece GetOne_espece(int id)
        {
            DBConnect dbtest = new DBConnect();

            Dictionary<string, object> e_brut = dbtest.getOne(id, "Espece", "Id_espece");

            Espece e = new Espece();

            e.Id_espece = (int)e_brut["Id_espece"];
            e.Nom_espece = e_brut["Nom_espece"].ToString();
            e.Coefficient = (double)e_brut["Coefficient"];
            e.Id_regime = (int)e_brut["Id_regime"];
            e.Id_envi = (int)e_brut["Id_envi"];
            e.Id_epoque = (int)e_brut["Id_epoque"];
            

            return e;
        }

        public static List<Espece> Get_espece()
        {
            DBConnect dbtest = new DBConnect();

            List<Dictionary<string, object>> liste_e_brut = dbtest.get("Espece");

            List<Espece> liste_e = new List<Espece>();


            foreach (var item in liste_e_brut)
            {

                Espece e = new Espece();

                e.Id_espece = (int)item["Id_espece"];
                e.Nom_espece = item["Nom_espece"].ToString();
                e.Coefficient = (double)item["Coefficient"];
                e.Id_regime = (int)item["Id_regime"];
                e.Id_envi = (int)item["Id_envi"];
                e.Id_epoque = (int)item["Id_epoque"];

                liste_e.Add(e);

            }
            return liste_e;
        }

        public static List<Espece> GetFilter_espece(string query_where, Dictionary<string, object> parametres)
        {
            DBConnect dbtest = new DBConnect();
            string query_filter = "SELECT * FROM Espece WHERE " + query_where;

            List<Dictionary<string, Object>> liste_e_filter_brute = dbtest.getFilter(query_filter, parametres);


            List<Espece> liste_e = new List<Espece>();


            foreach (var item in liste_e_filter_brute)
            {

                Espece e = new Espece();

                e.Id_espece = (int)item["Id_espece"];
                e.Nom_espece = item["Nom_espece"].ToString();
                e.Coefficient = (double)item["Coefficient"];
                e.Id_regime = (int)item["Id_regime"];
                e.Id_envi = (int)item["Id_envi"];
                e.Id_epoque = (int)item["Id_epoque"];

                liste_e.Add(e);

            }
            return liste_e;
        }
    }
}
