﻿using Interzoo.DAL;
using Interzoo.Models.ModelBilleterie;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interzoo.Models.Repository
{
    public static class Ticket_repo
    {

        public static int Insert_ticket(Ticket t)
        {
            DBConnect dbtest = new DBConnect();

            string query_insert = @"INSERT INTO [dbo].[Ticket] (Nom, Prenom, Prix,Id_cat, Id_reservation) 
                                   OUTPUT INSERTED.Id_ticket VALUES (@Nom, @Prenom, @Prix, @Id_cat, @Id_reservation)";

            Dictionary<string, object> parametres_insert = new Dictionary<string, object>();

            parametres_insert.Add("@Nom", t.Nom); //le nom avec ou sans @, le programme comprends les deux apparemment: dans le execute, les entrées du tableau seront données (la Key en Key et la Value en Value) au SqlParameter (=Dictionnaire) qui servira à remplacer les @nom par les valeurs du tableau SqlParameter dont la clef @Nom ou Nom correspond
            parametres_insert.Add("@Prenom", t.Prenom);
            parametres_insert.Add("@Prix", t.Prix);
            parametres_insert.Add("@Id_cat", t.Id_cat);
            parametres_insert.Add("@Id_reservation", t.Id_reservation);


            return dbtest.Insert(parametres_insert, query_insert);
        }
    }
}
