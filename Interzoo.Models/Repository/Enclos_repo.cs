﻿using Interzoo.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interzoo.Models.Repository
{
    public class Enclos_repo
    {
        public static Enclos GetOne_enclos(int id)
        {
            DBConnect dbtest = new DBConnect();

            Dictionary<string, object> e_brut = dbtest.getOne(id, "Enclos", "Id_enclos");

            Enclos e = new Enclos();

            e.Id_enclos= (int)e_brut["Id_enclos"];
            e.Taille = (double)e_brut["Taille"];
            e.Capacite_accueil = (double)e_brut["Capacite_accueil"];
            e.Id_envi = (int)e_brut["Id_envi"];
            e.Id_epoque = (int)e_brut["Id_epoque"];
            e.Details_enclos = e_brut["Details_enclos"] == DBNull.Value ? null : e_brut["Details_enclos"].ToString();

            return e;
        }

        public static List<Enclos> Get_enclos()
        {
            DBConnect dbtest = new DBConnect();

            List<Dictionary<string, object>> liste_e_brut = dbtest.get("Enclos");

            List<Enclos> liste_e = new List<Enclos>();


            foreach (var item in liste_e_brut)
            {
                Enclos e = new Enclos();
                e.Id_enclos = (int)item["Id_enclos"];
                e.Taille = (double)item["Taille"];
                e.Capacite_accueil = (double)item["Capacite_accueil"];
                e.Id_envi = (int)item["Id_envi"];
                e.Id_epoque = (int)item["Id_epoque"];
                e.Details_enclos = item["Details_enclos"] == DBNull.Value ? null : item["Details_enclos"].ToString();


                liste_e.Add(e);
            }

            return liste_e;
        }

        public static List<Enclos> GetFilter_enclos(string query_where, Dictionary<string, object> parametres)
        {
            DBConnect dbtest = new DBConnect();
            string query_filter = "SELECT * FROM Enclos WHERE " + query_where;

            List<Dictionary<string, Object>> liste_enclos_filter_brute = dbtest.getFilter(query_filter, parametres);

            List<Enclos> liste_e = new List<Enclos>(); 

            foreach (var item in liste_enclos_filter_brute)  
            {
                Enclos e = new Enclos();
                e.Id_enclos = (int)item["Id_enclos"];
                e.Taille = (double)item["Taille"];
                e.Capacite_accueil = (double)item["Capacite_accueil"];
                e.Id_envi = (int)item["Id_envi"];
                e.Id_epoque = (int)item["Id_epoque"];
                e.Details_enclos = item["Details_enclos"] == DBNull.Value ? null : item["Details_enclos"].ToString();


                liste_e.Add(e);
            }

            return liste_e;
        }

        public static int Delete_enclos(int id) // on connait déjà le nom de la table et de la clonne_id --> on ne doit préciser que l'id
        {
            DBConnect dbtest = new DBConnect();

            return dbtest.Delete(id, "Enclos", "Id_enclos");
        }

        public static int Insert_enclos(Enclos e)
        {
            DBConnect dbtest = new DBConnect();

            string query_insert = @"INSERT INTO [dbo].[Enclos] (Taille, Capacite_accueil, Id_envi, Id_epoque, Details_enclos) 
                                   OUTPUT INSERTED.Id_enclos VALUES (@Taille, @Capacite_accueil, @Id_envi, @Id_epoque, @Details_enclos)";

            Dictionary<string, object> parametres_insert = new Dictionary<string, object>();

            parametres_insert.Add("@Taille", e.Taille); //le nom avec ou sans @, le programme comprends les deux apparemment: dans le execute, les entrées du tableau seront données (la Key en Key et la Value en Value) au SqlParameter (=Dictionnaire) qui servira à remplacer les @nom par les valeurs du tableau SqlParameter dont la clef @Nom ou Nom correspond
            parametres_insert.Add("@Capacite_accueil", e.Capacite_accueil);
            parametres_insert.Add("@Id_envi", e.Id_envi);
            parametres_insert.Add("@Id_epoque", e.Id_epoque);
            parametres_insert.Add("@Details_enclos", e.Details_enclos);

            return dbtest.Insert(parametres_insert, query_insert);
        }

        public static int Update_enclos(Enclos e)  //l'animal tel qu'il apparaitra dans base de données (valeurs qui restent reprise + valeurs changées changées)
        {
            DBConnect dbtest = new DBConnect();

            string query_insert = @"UPDATE [dbo].[Enclos] 
                                    SET taille = @Taille, Capacite_accueil = @Capacite_accueil, Id_envi = @Id_envi, Id_epoque = @Id_epoque, Details_enclos = @Details_enclos
                                    OUTPUT INSERTED.Id_enclos
                                    WHERE Id_enclos = @Id_enclos";

            Dictionary<string, object> parametres_insert = new Dictionary<string, object>();

            //on rempli le Dictionnaire nouvellement créé avec, en clé, le string qui indique quelle valeur en @ de la query sera remplacée (automatiquement) en en value, la valeur par laquelle le @... sera remplacé
            //ici cette valeur est trouvée grace aux propriétés de la Peronne p passée en paramètre
            parametres_insert.Add("@Taille", e.Taille); //le nom avec ou sans @, le programme comprends les deux apparemment: dans le execute, les entrées du tableau seront données (la Key en Key et la Value en Value) au SqlParameter (=Dictionnaire) qui servira à remplacer les @nom par les valeurs du tableau SqlParameter dont la clef @Nom ou Nom correspond
            parametres_insert.Add("@Capacite_accueil", e.Capacite_accueil);
            parametres_insert.Add("@Id_envi", e.Id_envi);
            parametres_insert.Add("@Id_epoque", e.Id_epoque);
            parametres_insert.Add("@Details_enclos", e.Details_enclos);
            parametres_insert.Add("@Id_enclos", e.Id_enclos);

            return dbtest.Insert(parametres_insert, query_insert);
        }
    }
}
