﻿using Interzoo.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interzoo.Models.Repository
{
    public class Animal_repo
    {
        public static List<Animal> Get_animal()
        {
            DBConnect dbtest = new DBConnect();

            List<Dictionary<string, object>> liste_animaux_brut = dbtest.get("Animal");

            List<Animal> liste_animal = new List<Animal>();


            foreach (var item in liste_animaux_brut)
            {
                Animal a = new Animal();
                a.ID_animal = (int)item["ID_animal"];
                a.Nom = item["Nom"].ToString();
                a.Date_naiss = (DateTime)item["Date_naiss"];
                //a.Espece = item["Espece"].ToString();
                a.Genre = item["Genre"] == DBNull.Value ? null : item["Genre"].ToString();
                a.Description = item["Description"] == DBNull.Value ? null : item["Description"].ToString();
                a.Picture = item["Picture"] == DBNull.Value ? null : item["Picture"].ToString();
                a.Id_espece = (int)item["Id_espece"];
                a.Id_enclos = (int)item["Id_enclos"];


                liste_animal.Add(a);
            }

            return liste_animal;
        }

        public static Animal GetOne_animal(int id)
        {
            DBConnect dbtest = new DBConnect();

            Dictionary<string, object> a_brut = dbtest.getOne(id, "Animal", "ID_animal");

            Animal a = new Animal();
            a.ID_animal = (int)a_brut["ID_animal"];
            a.Nom = a_brut["Nom"].ToString();
            a.Date_naiss = (DateTime)a_brut["Date_naiss"];
            //a.Espece = item["Espece"].ToString();
            a.Genre = a_brut["Genre"] == DBNull.Value ? null : a_brut["Genre"].ToString();
            a.Description = a_brut["Description"] == DBNull.Value ? null : a_brut["Description"].ToString();
            a.Picture = a_brut["Picture"] == DBNull.Value ? null : a_brut["Picture"].ToString();
            a.Id_espece = (int)a_brut["Id_espece"];
            a.Id_enclos = (int)a_brut["Id_enclos"];

            return a;
        }

        public static List<Animal> GetFilter_animal(string query_where, Dictionary<string, object> parametres)
        {
            DBConnect dbtest = new DBConnect();
            string query_filter = "SELECT * FROM Animal WHERE " + query_where; 

            List<Dictionary<string, Object>> liste_animals_filter_brute = dbtest.getFilter(query_filter, parametres); //liste de Dictionnaires to be convertis en personnes

            List<Animal> liste_gifts = new List<Animal>(); //liste_personnes de personne vide ou l'on mettre les new personnes créées (une par Dictionnaire de la liste)

            foreach (var item in liste_animals_filter_brute)  //on attribue les values de chaque Dictionnaires de la liste aux propriétées d'une personne nouvellement créée (on utilise la clef/index du dictionnaire traité pour attribuer la bonne valeur à la bonne propriété (!faire correspondre les noms))
            {
                Animal a = new Animal();
                a.ID_animal = (int)item["ID_animal"];
                a.Nom = item["Nom"].ToString();
                a.Date_naiss = (DateTime)item["Date_naiss"];
                //a.Espece = item["Espece"].ToString();
                a.Genre = item["Genre"] == DBNull.Value ? null : item["Genre"].ToString();
                a.Description = item["Description"] == DBNull.Value ? null : item["Description"].ToString();
                a.Picture = item["Picture"] == DBNull.Value ? null : item["Picture"].ToString();
                a.Id_espece = (int)item["Id_espece"];
                a.Id_enclos = (int)item["Id_enclos"];

                liste_gifts.Add(a);
            }

            return liste_gifts;
        }

        public static int Delete_animal(int id) // on connait déjà le nom de la table et de la clonne_id --> on ne doit préciser que l'id
        {
            DBConnect dbtest = new DBConnect();

            return dbtest.Delete(id, "Animal", "ID_animal");
        }

        public static int Insert_animal(Animal a)
        {
            DBConnect dbtest = new DBConnect();

            string query_insert = @"INSERT INTO [dbo].[Animal] (Nom, Picture, Date_naiss, Genre, Description, Id_enclos, Id_espece) 
                                   OUTPUT INSERTED.ID_animal VALUES (@Nom, @Picture, @Date_naiss, @genre, @Description, @Id_enclos, @Id_espece)";

            Dictionary<string, object> parametres_insert = new Dictionary<string, object>();

            parametres_insert.Add("@Nom", a.Nom); //le nom avec ou sans @, le programme comprends les deux apparemment: dans le execute, les entrées du tableau seront données (la Key en Key et la Value en Value) au SqlParameter (=Dictionnaire) qui servira à remplacer les @nom par les valeurs du tableau SqlParameter dont la clef @Nom ou Nom correspond
            parametres_insert.Add("@Picture", a.Picture);
            parametres_insert.Add("@Date_naiss", a.Date_naiss);
            parametres_insert.Add("@Genre", a.Genre);
            parametres_insert.Add("@Description", a.Description);
            parametres_insert.Add("@Id_enclos", a.Id_enclos);
            parametres_insert.Add("@Id_espece", a.Id_espece);


            return dbtest.Insert(parametres_insert, query_insert);
        }

        public static int Update_animal(Animal a)  //l'animal tel qu'il apparaitra dans base de données (valeurs qui restent reprise + valeurs changées changées)
        {
            DBConnect dbtest = new DBConnect();

            string query_insert = @"UPDATE [dbo].[Animal] 
                                    SET Nom = @Nom, Picture = @Picture, Date_naiss = @Date_naiss, Genre = @Genre, Description = @Description, Id_enclos = @Id_enclos, Id_espece = @Id_espece
                                    OUPUT INSERTED.ID_animal
                                    WHERE ID_animal = @ID_animal";

            Dictionary<string, object> parametres_insert = new Dictionary<string, object>();

            //on rempli le Dictionnaire nouvellement créé avec, en clé, le string qui indique quelle valeur en @ de la query sera remplacée (automatiquement) en en value, la valeur par laquelle le @... sera remplacé
            //ici cette valeur est trouvée grace aux propriétés de la Peronne p passée en paramètre
            parametres_insert.Add("@Nom", a.Nom); //le nom avec ou sans @, le programme comprends les deux apparemment: dans le execute, les entrées du tableau seront données (la Key en Key et la Value en Value) au SqlParameter (=Dictionnaire) qui servira à remplacer les @nom par les valeurs du tableau SqlParameter dont la clef @Nom ou Nom correspond
            parametres_insert.Add("@Picture", a.Picture);
            parametres_insert.Add("@Date_naiss", a.Date_naiss);
            parametres_insert.Add("@Genre", a.Genre);
            parametres_insert.Add("@Description", a.Description);
            parametres_insert.Add("@Id_enclos", a.Id_enclos);
            parametres_insert.Add("@Id_espece", a.Id_espece);
            parametres_insert.Add("@ID_animal", a.ID_animal);



            return dbtest.Insert(parametres_insert, query_insert);
        }
    }
}
