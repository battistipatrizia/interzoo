﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interzoo.Models
{
    public class Adulte : Visiteur
    {
        public Adulte(string nom, string prenom, DateTime date_naiss) : base(nom, prenom, date_naiss)
        {
        }
    }
}
