﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interzoo.Models
{
    abstract public class Personnel : Personne
    {

        private double _salaire;
        private int _anciennete;
        private string _coordonnees;
        private string _matricule;


        public Personnel(string nom, string prenom, DateTime date_naiss, double salaire, int anciennete, string coordonnees, string matricule) : base(nom, prenom, date_naiss)
        {
            Salaire = salaire;
            Anciennete = anciennete;
            Coordonnees = coordonnees;
            Matricule = matricule;

        }

        public double Salaire
        {
            get { return _salaire; }
            set { _salaire = value; }
        }

        public int Anciennete
        {
            get { return _anciennete; }
            set { _anciennete = value; }
         }

        public string Coordonnees
        {
            get { return _coordonnees; }
            set { _coordonnees = value; }
        }

        public string Matricule
        {
            get { return _matricule; }
            set { _matricule = value; }
        }
    }
  }
