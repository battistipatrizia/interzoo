﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interzoo.Models.ModelBilleterie
{
    public class Reservation
    {
        private int id_Reservation;
        private int idPersonne;
        private DateTime date_reservation;
        private int nbr_ticket;

        public int Id_Reservation
        {
            get
            {
                return id_Reservation;
            }

            set
            {
                id_Reservation = value;
            }
        }

        public int IdPersonne
        {
            get
            {
                return idPersonne;
            }

            set
            {
                idPersonne = value;
            }
        }

        public DateTime Date_reservation
        {
            get
            {
                return date_reservation;
            }

            set
            {
                date_reservation = value;
            }
        }

        public int Nbr_ticket
        {
            get
            {
                return nbr_ticket;
            }

            set
            {
                nbr_ticket = value;
            }
        }
    }
}
