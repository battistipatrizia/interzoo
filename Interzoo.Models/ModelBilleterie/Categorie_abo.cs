﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interzoo.Models.ModelBilleterie
{
    public class Categorie_abo
    {
        private int id_cat;
        private string nom_cat;
        private double prix_abo;
        private string details;

        public Categorie_abo() { }

        public Categorie_abo(int id_cat, string nom_cat, double prix_abo, string details)
        {
            this.id_cat = id_cat;
            this.nom_cat = nom_cat;
            this.Prix_abo = prix_abo;
            this.details = details;
        }

        public int Id_cat
        {
            get
            {
                return id_cat;
            }

            set
            {
                id_cat = value;
            }
        }

        public string Nom_cat
        {
            get
            {
                return nom_cat;
            }

            set
            {
                nom_cat = value;
            }
        }


        public string Details
        {
            get
            {
                return details;
            }

            set
            {
                details = value;
            }
        }

        public double Prix_abo
        {
            get
            {
                return prix_abo;
            }

            set
            {
                prix_abo = value;
            }
        }
    }
}
