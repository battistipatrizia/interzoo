﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interzoo.Models.ModelBilleterie
{
    public class Categorie_ticket
    {
        private int id_cat;
        private string nom_cat;
        private double prix_ticket;
        private double prix_abo;
        private string details;

        public Categorie_ticket() { }

        public Categorie_ticket(int id_cat, string nom_cat, double prix_ticket, string details)
        {
            this.id_cat = id_cat;
            this.nom_cat = nom_cat;
            this.prix_ticket = prix_ticket;
            this.details = details;
        }

        public int Id_cat
        {
            get
            {
                return id_cat;
            }

            set
            {
                id_cat = value;
            }
        }

        public string Nom_cat
        {
            get
            {
                return nom_cat;
            }

            set
            {
                nom_cat = value;
            }
        }

        public double Prix_ticket
        {
            get
            {
                return prix_ticket;
            }

            set
            {
                prix_ticket = value;
            }
        }


        public string Details
        {
            get
            {
                return details;
            }

            set
            {
                details = value;
            }
        }

        public double Prix_abo
        {
            get
            {
                return prix_abo;
            }

            set
            {
                prix_abo = value;
            }
        }
    }
}
