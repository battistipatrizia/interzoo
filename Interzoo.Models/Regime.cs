﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interzoo.Models
{
    public class Regime
    {

        private int _id_regime;
        private string _type_regime;
        private string _description_regime;

        public Regime()
        { }

        public Regime(int id_regime, string type_regime, string description_regime)
        {
            this._id_regime = id_regime;
            this._type_regime = type_regime;
            this._description_regime = description_regime;
        }

        public int Id_regime
        {
            get
            {
                return _id_regime;
            }

            set
            {
                _id_regime = value;
            }
        }

        public string Type_regime
        {
            get
            {
                return _type_regime;
            }

            set
            {
                _type_regime = value;
            }
        }

        public string Description_regime
        {
            get
            {
                return _description_regime;
            }

            set
            {
                _description_regime = value;
            }
        }
    }
}
